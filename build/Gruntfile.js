// Generated on 2014-03-28 using generator-phaser-official 0.0.8-rc-2
'use strict';
var config = require('./config.json');
var _ = require('underscore');
_.str = require('underscore.string');

// Mix in non-conflict functions to Underscore namespace if you want
_.mixin(_.str.exports());

var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({
    port: LIVERELOAD_PORT
});
var mountFolder = function(connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function(grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        clean: {
            build: {
                src: ["assets/compressed", "dist/assets"]
            }
        },
        watch: {
            scripts: {
                files: [
                    'game/**/*.js',
                    '!game/main.js'
                ],
                options: {
                    spawn: false,
                    livereload: LIVERELOAD_PORT
                },
                tasks: ['build']
            }
        },
        connect: {
            options: {
                port: 9000,
                // change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    middleware: function(connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, 'dist')
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                path: 'http://localhost:9000'
            }
        },
        copy: {
            dist: {
                files: [
                    // includes files within path and its sub-directories
                    {
                        expand: true,
                        flatten: true,
                        src: ['game/plugins/*.js'],
                        dest: 'dist/js/plugins/'
                    }, {
                        expand: true,
                        flatten: true,
                        src: ['bower_components/**/build/*.js'],
                        dest: 'dist/js/'
                    }, {
                        expand: true,
                        src: ['css/**'],
                        dest: 'dist/'
                    }, {
                        expand: true,
                        src: ['index.html'],
                        dest: 'dist/'
                    }
                ]
            }
        },
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    // Set to true to enable the following options…
                    expand: true,
                    // cwd is 'current working directory'
                    cwd: 'assets/',
                    src: ['**/*.png'],
                    // Could also match cwd line above. i.e. project-directory/img/
                    dest: 'assets/compressed/',
                    ext: '.png'
                }]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [{
                    // Set to true to enable the following options…
                    expand: true,
                    // cwd is 'current working directory'
                    cwd: 'assets/',
                    src: ['**/*.jpg'],
                    // Could also match cwd. i.e. project-directory/img/
                    dest: 'assets/compressed/',
                    ext: '.jpg'
                }]
            }
        },
        responsive_images: {
            variables: {
                options: {
                    sizes: [{
                        name: "small",
                        width: '50%',
                        height: '50%'
                    }, {
                        name: "medium",
                        width: '66.6666666666667%',
                        height: '66.6666666666667%'
                    }, {
                        name: "large",
                        width: "100%",
                        height: '100%'
                    }]
                },
                files: [{
                    expand: true,
                    cwd: 'assets/compressed/',
                    src: ['**/*.{jpg,gif,png}'],
                    dest: 'dist/assets/'
                }]
            }
        },
        browserify: {
            build: {
                src: ['game/main.js'],
                dest: 'dist/js/game.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-responsive-images');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('build', ['buildBootstrapper', 'browserify', 'copy']);
    grunt.registerTask('stage', ['clean', 'buildBootstrapper', 'browserify', 'copy', 'imagemin', 'responsive_images']);
    grunt.registerTask('serve', ['build', 'connect:livereload', 'open', 'watch']);
    grunt.registerTask('default', ['serve']);
    grunt.registerTask('prod', ['build', 'copy']);

    grunt.registerTask('buildBootstrapper', 'builds the bootstrapper file correctly', function() {
        var stateFiles = grunt.file.expand('game/states/*.js');
        var gameStates = [];
        var statePattern = new RegExp(/(\w+).js$/);
        stateFiles.forEach(function(file) {
            var state = file.match(statePattern)[1];
            if (!!state) {
                gameStates.push({
                    shortName: state,
                    stateName: _.capitalize(state) + 'State'
                });
            }
        });
        config.gameStates = gameStates;
        console.log(config);
        var bootstrapper = grunt.file.read('templates/_main.js.tpl');
        bootstrapper = grunt.template.process(bootstrapper, {
            data: config
        });
        grunt.file.write('game/main.js', bootstrapper);
    });
};
