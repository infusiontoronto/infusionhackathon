'use strict';

//global variables
window.onload = function () {
	var suffix = "large";
	if (<%= gameHeight %> == 720) {
		suffix = "medium";
	} else if ( <%= gameHeight %> == 540) {
		suffix = "small";
	}
  var game = new Phaser.Game(<%= gameWidth %>, <%= gameHeight %>, Phaser.AUTO, '<%= _.slugify(projectName) %>');

  game.suffix = suffix;

  // Game States
  <% _.forEach(gameStates, function(gameState) {  %>game.state.add('<%= gameState.shortName %>', require('./states/<%= gameState.shortName %>'));
  <% }); %>

  game.state.start('boot');


};