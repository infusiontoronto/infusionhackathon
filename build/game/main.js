'use strict';

//global variables
window.onload = function () {
	var suffix = "large";
	if (1080 == 720) {
		suffix = "medium";
	} else if ( 1080 == 540) {
		suffix = "small";
	}
  var game = new Phaser.Game(1920, 1080, Phaser.AUTO, 'beerrun');

  game.suffix = suffix;

  // Game States
  game.state.add('boot', require('./states/boot'));
  game.state.add('gameover', require('./states/gameover'));
  game.state.add('howto', require('./states/howto'));
  game.state.add('leaderboard', require('./states/leaderboard'));
  game.state.add('menu', require('./states/menu'));
  game.state.add('pauseState', require('./states/pauseState'));
  game.state.add('play', require('./states/play'));
  game.state.add('preload', require('./states/preload'));
  game.state.add('share', require('./states/share'));
  

  game.state.start('boot');


};