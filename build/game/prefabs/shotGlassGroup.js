'use strict';

var ShotGlassObstacle = require('./shotGlassObstacle');

var yPos = 875;

var ShotGlassGroup = function(game, parent) {

    Phaser.Group.call(this, game, parent);

    // this.topPipe = new Pipe(this.game, 0, 0, 0);
    this.obstacle = new ShotGlassObstacle(this.game, 0, yPos);
    // this.add(this.topPipe);
    this.add(this.obstacle);
    this.hasScored = false;

    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

ShotGlassGroup.prototype = Object.create(Phaser.Group.prototype);
ShotGlassGroup.prototype.constructor = ShotGlassGroup;

ShotGlassGroup.prototype.update = function() {
    this.checkWorldBounds();

    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

ShotGlassGroup.prototype.checkWorldBounds = function() {
    if (!this.obstacle.inWorld) {
        this.exists = false;
    }
};

ShotGlassGroup.prototype.stopMoving = function() {
    this.setAll('body.velocity.x', 0);
}

ShotGlassGroup.prototype.startMoving = function() {
    this.setAll('body.velocity.x', -200);
}

ShotGlassGroup.prototype.hit = function() {
    this.obstacle.animations.frame = 1;
    // var t = this.game.add.tween(this.obstacle).to( { y: 1480, x: 1800 }, 2000, Phaser.Easing.Linear.None, false, 0, 1, false);
    // this.obstacle.body.immovable = true;
    // this.game.physics.arcade.enableBody(this.obstacle);
    // t.onComplete.addOnce(this.reset, this);
    this.reset();
    // t.start();
}

ShotGlassGroup.prototype.reset = function(x, y) {
    // this.topPipe.reset(0,0);
    this.obstacle.reset(0, yPos);
    this.x = x;
    this.y = y;
    this.setAll('body.velocity.x', -200);
    this.hasScored = false;
    this.exists = true;
};


module.exports = ShotGlassGroup;
