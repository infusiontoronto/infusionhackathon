/* Full tutorial: http://codevinsky.ghost.io/phaser-2-0-tutorial-flappy-bird-part-4/ */
'use strict';

var BarflyObstacle = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'barfly', frame);
    this.anchor.setTo(0.5, 0.5);
    this.game.physics.arcade.enableBody(this);

    this.body.allowGravity = false;
    this.body.immovable = true;

    this.game.add.tween(this).to({
        y: this.y + 30
    }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, 100, true);

};

BarflyObstacle.prototype = Object.create(Phaser.Sprite.prototype);
BarflyObstacle.prototype.constructor = BarflyObstacle;

BarflyObstacle.prototype.update = function() {
    // write your prefab's specific update code here

};

BarflyObstacle.prototype.hit = function() {
    this.reset();
}

module.exports = BarflyObstacle;
