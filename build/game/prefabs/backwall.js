'use strict';
var WallArt = require('./wallArt');

var justHit = false;
var scrollSpeed = -150;
var prevScrollPos = 0;

var Backwall = function(game, x, y, width, height) {
    Phaser.TileSprite.call(this, game, x, y, width, height, 'background');

    this.autoScroll(scrollSpeed, 0);
};

Backwall.prototype = Object.create(Phaser.TileSprite.prototype);
Backwall.prototype.constructor = Backwall;

Backwall.prototype.update = function() {
    this.scrollSpeed = this.game.scrollSpeed * 0.75;
    this.autoScroll(this.scrollSpeed, 0);
};

Backwall.prototype.stopMoving = function() {
    this.autoScroll(0, 0);
}


Backwall.prototype.hit = function() {
    this.scrollSpeed = 0;
    this.stopMoving();
    justHit = true;
}


Backwall.prototype.startMoving = function() {
    this.autoScroll(scrollSpeed, 0);
}

module.exports = Backwall;
