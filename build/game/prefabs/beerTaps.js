'use strict';

var initialX;

var BeerTaps = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'beerTaps', frame);

    initialX = x;

    this.game.physics.arcade.enableBody(this);
    this.anchor.setTo(0, 0.55);
    this.body.allowGravity = false;
    this.body.immovable = true;
};

BeerTaps.prototype = Object.create(Phaser.Sprite.prototype);
BeerTaps.prototype.constructor = BeerTaps;

BeerTaps.prototype.update = function() {

    // write your prefab's specific update code here
    //this.body.velocity.x = this.game.scrollSpeed;

};

BeerTaps.prototype.show = function() {
    this.body.velocity.x = this.game.scrollSpeed;
}

BeerTaps.prototype.reset = function() {
    this.body.x = initialX;
    this.body.velocity.x = 0;
}

module.exports = BeerTaps;
