'use strict';

var PowerUpGroup = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'powerUpGroup', frame);

    // initialize your prefab here

};

PowerUpGroup.prototype = Object.create(Phaser.Sprite.prototype);
PowerUpGroup.prototype.constructor = PowerUpGroup;

PowerUpGroup.prototype.update = function() {

    // write your prefab's specific update code here

};

module.exports = PowerUpGroup;
