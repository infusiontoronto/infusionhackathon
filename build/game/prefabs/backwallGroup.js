/* Full tutorial: http://codevinsky.ghost.io/phaser-2-0-tutorial-flappy-bird-part-4/ */
'use strict';

var Backwall = require('./backwall');
var WallArt = require('./wallArt');
var prevScrollPos = 0;
var BackwallGroup = function(game, parent) {

    Phaser.Group.call(this, game, parent);

    this.bg = new Backwall(this.game, 0, 0, 1920, 1080);
    this.add(this.bg);

    var artId = this.game.rnd.integerInRange(1, 5);
    this.art1 = new WallArt(this.game, 1440, 0, artId);
    this.add(this.art1);

    artId = this.game.rnd.integerInRange(1, 5);
    this.art2 = new WallArt(this.game, 3260, 0, artId);
    this.add(this.art2);


};

BackwallGroup.prototype = Object.create(Phaser.Group.prototype);
BackwallGroup.prototype.constructor = BackwallGroup;

BackwallGroup.prototype.update = function() {
    if (Math.abs(this.bg.tilePosition.x - prevScrollPos) > 1000) {

        this.updateWallArt();
    }

    prevScrollPos = this.bg.tilePosition.x;
};

BackwallGroup.prototype.updateWallArt = function() {

    if (this.art1.body.x < 0) {
        this.art1.body.x = 2900;
    } else if (this.art2.body.x < 0) {
        this.art2.body.x = 2900;
    }

};

BackwallGroup.prototype.checkWorldBounds = function() {
    if (!this.obstacle.inWorld) {
        this.exists = false;
    }
};

BackwallGroup.prototype.stopMoving = function() {
    this.setAll('body.velocity.x', 0);
}

BackwallGroup.prototype.startMoving = function() {
    this.setAll('body.velocity.x', -200);
}

BackwallGroup.prototype.hit = function() {
    this.reset();
}

BackwallGroup.prototype.reset = function(x, y) {
    // this.topPipe.reset(0,0);
    // this.art.reset(0,440);
    this.x = x;
    this.y = y;
    // this.setAll('body.velocity.x', -200);
    // this.hasScored = false;
    this.exists = true;
};


module.exports = BackwallGroup;
