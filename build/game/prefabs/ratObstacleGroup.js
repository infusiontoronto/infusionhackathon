'use strict';

var RatObstacle = require('./ratObstacle');

var yPos = 875;

var RatObstacleGroup = function(game, parent) {

    Phaser.Group.call(this, game, parent);
    this.obstacle = new RatObstacle(this.game, 0, yPos);
    this.add(this.obstacle);
    this.hasScored = false;

    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

RatObstacleGroup.prototype = Object.create(Phaser.Group.prototype);
RatObstacleGroup.prototype.constructor = RatObstacleGroup;

RatObstacleGroup.prototype.update = function() {
    this.checkWorldBounds();
    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

RatObstacleGroup.prototype.checkWorldBounds = function() {
    if (!this.obstacle.inWorld) {
        this.exists = false;
    }
};

RatObstacleGroup.prototype.stopMoving = function() {
    this.setAll('body.velocity.x', 0);
}

RatObstacleGroup.prototype.startMoving = function() {
    this.setAll('body.velocity.x', -200);
}

RatObstacleGroup.prototype.hit = function() {
    this.reset();
}

RatObstacleGroup.prototype.obstacleType = function() {
    return "rat";
}

RatObstacleGroup.prototype.obstacleDamage = function() {
    return 5;
}

RatObstacleGroup.prototype.hitMessage = function() {
    return "HitRat";
}

RatObstacleGroup.prototype.reset = function(x, y) {
    // this.topPipe.reset(0,0);
    this.obstacle.reset(0, yPos);
    this.x = x;
    this.y = y;
    this.setAll('body.velocity.x', -200);
    this.hasScored = false;
    this.exists = true;
};


module.exports = RatObstacleGroup;
