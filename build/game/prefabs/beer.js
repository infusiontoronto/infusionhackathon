'use strict';
var barLevel = 712;
var beerSpillEmitter;
var justHit = false;

var Beer = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'beer', frame);

    // set the sprite's anchor to the center
    this.anchor.setTo(0.5, 0.5);
    this.flipTween = null;
    // add and play animations
    this.animations.add('jump');
    this.animations.play('jump', 12, true);
    this.animations.stop();

    this.game.physics.arcade.enableBody(this);

    beerSpillEmitter = this.game.add.emitter(this.game.world.centerX, 330, 4000);
    beerSpillEmitter.makeParticles(['beerParticle1', 'beerParticle2', 'beerParticle3', 'beerParticle4']);
    beerSpillEmitter.gravity = 400;
    beerSpillEmitter.width = 40;
    beerSpillEmitter.setAlpha(1, 0, 3000);
    beerSpillEmitter.setScale(0.2, 0.1, 0.2, 0.1, 3000);

};

Beer.prototype = Object.create(Phaser.Sprite.prototype);
Beer.prototype.constructor = Beer;

Beer.prototype.update = function() {

    if (this.angle < 0) {
        this.angle += 2.5;
    }


    if (justHit) {
        if (this.body.x < this.game.world.centerX) {
            this.body.velocity.x += 8;
        } else {
            this.body.x = this.game.world.centerX;
            this.body.velocity.x = 0;
            justHit = false;
        }

    }



};

Beer.prototype.hit = function() {
    beerSpillEmitter.y = this.body.y;
    beerSpillEmitter.start(true, 2000, null, 15);
    this.game.add.tween(this).to({
        angle: -15
    }, 500, Phaser.Easing.Linear.None, true, 0, 1, true);
    justHit = true;
}

Beer.prototype.jump = function() {
    this.jumped = true;
    if (this.body.y == barLevel) {

        this.body.velocity.y = -900;
        this.game.add.tween(this).to({
            angle: -40
        }, 100).start();

    } else {

        this.flipTween = this.game.add.tween(this).to({
            angle: 360
        }, 500);
        this.flipTween.start();
    }

};

Beer.prototype.updateFillAmount = function(amt) {
    // TODO: go to appropriate frame of beer spritesheet

    this.animations.frame = Math.min(6 - Math.round((amt / 100) * 6), 6);
},

Beer.prototype.refill = function() {
    // TODO: go to the full frame of beer spritesheet

}

Beer.prototype.spawn = function() {

    var destX = this.game.world.centerX;
    this.body.x = -100;
    this.game.add.tween(this).to({
        x: destX
    }, 1000, Phaser.Easing.Quadratic.Out).start();

}

module.exports = Beer;
