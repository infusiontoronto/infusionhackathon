'use strict';

var curLevel = 0;
var curBeerAmount = 100;
var score = 0;
var paused = false;
var HUD = function(game, parent) {
    Phaser.Group.call(this, game, parent);
    // Phaser.Sprite.call(this, game, x, y, 'HUD', frame);

    // initialize your prefab here
    // this.background.alpha = 0;
    this.parent = parent;

    var style = {
        font: "69px Arial",
        fill: "#ffffff",
        align: "center"
    };
    this.levelLabel = this.game.add.image(670, 50, 'hud-level');
    this.levelLabel.anchor.setTo(0.5);
    this.add(this.levelLabel);

    this.levelValueText = this.game.add.text(650, 75, "1", style);
    this.add(this.levelValueText);

    this.beerMeterLabel = this.game.add.image(1350, 50, 'hud-beer-meter');
    this.beerMeterLabel.anchor.setTo(0.5);
    this.add(this.beerMeterLabel);

    this.scoreLabel = this.game.add.image(320, 50, 'hud-score');
    this.scoreLabel.anchor.setTo(0.5);
    this.add(this.scoreLabel);

    this.scoreValueText = this.game.add.text(260, 75, "0", style);
    this.add(this.scoreValueText);

    this.btnHudPause = this.game.add.button(100, 100, 'hud-pause', this.pauseClick, this);
    this.btnHudPause.anchor.setTo(0.5, 0.5);
    this.add(this.btnHudPause);

    this.healthBar = this.game.add.image(1100, 100, 'hud-health-bar');
    this.healthBar.scale.setTo(0.75);
    this.add(this.healthBar);

    this.health = this.game.add.sprite(1100, 100, 'hud-health');
    this.health.scale.setTo(0.75);
    this.add(this.health);

    //game.time.events.repeat(Phaser.Timer.SECOND*2,300,this.updateScore,this);

};

HUD.prototype = Object.create(Phaser.Group.prototype);
HUD.prototype.constructor = HUD;

HUD.prototype.update = function() {

    // write your prefab's specific update code here
    //this.levelText.text = "level: " + curLevel;
    //this.beerMeterText.text = "beer: " + curBeerAmount;


};

HUD.prototype.updateLevel = function(level) {
    curLevel = level;
    this.levelValueText.setText(curLevel);
}

HUD.prototype.isPaused = function() {
    return paused;
}
HUD.prototype.unpause = function() {
    paused = false;
}

HUD.prototype.updateBeerAmount = function(amount) {
    curBeerAmount = amount;
    if (curBeerAmount > 0) {
        this.health.width = (curBeerAmount / 100) * this.healthBar.width;
    }
}

HUD.prototype.updateScore = function(score) {
    this.scoreValueText.setText(score);
}

HUD.prototype.pauseClick = function() {
    paused = true;
}

module.exports = HUD;
