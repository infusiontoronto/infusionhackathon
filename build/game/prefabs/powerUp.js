'use strict';

var PowerUp = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'powerUp', frame);

    // initialize your prefab here

};

PowerUp.prototype = Object.create(Phaser.Sprite.prototype);
PowerUp.prototype.constructor = PowerUp;

PowerUp.prototype.update = function() {

    // write your prefab's specific update code here

};

module.exports = PowerUp;
