'use strict';

var BarflyObstacle = require('./barflyObstacle');

var yPos = 475;

var BarflyObstacleGroup = function(game, parent) {

    Phaser.Group.call(this, game, parent);

    // this.topPipe = new Pipe(this.game, 0, 0, 0);
    this.obstacle = new BarflyObstacle(this.game, 0, yPos);
    // this.add(this.topPipe);
    this.add(this.obstacle);
    this.hasScored = false;

    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

BarflyObstacleGroup.prototype = Object.create(Phaser.Group.prototype);
BarflyObstacleGroup.prototype.constructor = BarflyObstacleGroup;

BarflyObstacleGroup.prototype.update = function() {
    this.checkWorldBounds();

    this.setAll('body.velocity.x', this.game.scrollSpeed);
};

BarflyObstacleGroup.prototype.checkWorldBounds = function() {
    if (!this.obstacle.inWorld) {
        this.exists = false;
    }
};

BarflyObstacleGroup.prototype.stopMoving = function() {
    this.setAll('body.velocity.x', 0);
}

BarflyObstacleGroup.prototype.startMoving = function() {
    this.setAll('body.velocity.x', -200);
}

BarflyObstacleGroup.prototype.hit = function() {

    this.reset()

}

BarflyObstacleGroup.prototype.obstacleType = function() {
    return "barfly";
}

BarflyObstacleGroup.prototype.obstacleDamage = function() {
    return 3;
}

BarflyObstacleGroup.prototype.hitMessage = function() {
    return "HitFly";
}

BarflyObstacleGroup.prototype.reset = function(x, y) {
    // this.topPipe.reset(0,0);
    this.obstacle.reset(0, yPos);
    this.x = x;
    this.y = y;
    this.setAll('body.velocity.x', -200);
    this.hasScored = false;
    this.exists = true;
};


module.exports = BarflyObstacleGroup;
