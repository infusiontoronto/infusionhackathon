'use strict';

var Message = function(game, x, y, id, frame) {
    Phaser.Sprite.call(this, game, x, y, id, frame);
    this.anchor.setTo(0.5, 0.5);
    this.alpha = 0;
    this.scale.setTo(0.5, 0.5);
};

Message.prototype = Object.create(Phaser.Sprite.prototype);
Message.prototype.constructor = Message;

Message.prototype.update = function() {
    // write your prefab's specific update code here

};

Message.prototype.play = function() {
    this.alpha = 0;
    this.y = this.game.world.centerY;

    var t1 = this.game.add.tween(this.scale);
    t1.to({
        x: 1,
        y: 1
    }, 1000, Phaser.Easing.Exponential.Out, true, 0, 1, true);

    var t2 = this.game.add.tween(this).to({
        alpha: 1,
        y: this.y - 150
    }, 1000, Phaser.Easing.Back.Out, true, 0, 1, true);
    t2.onComplete.add(function() {
        this.y = this.game.world.height + 300;
    }, this);
}

module.exports = Message;
