/* Full tutorial: http://codevinsky.ghost.io/phaser-2-0-tutorial-flappy-bird-part-4/ */
'use strict';

var WallArt = function(game, x, y, id, frame) {
    Phaser.Sprite.call(this, game, x, y, 'background-art-' + id, frame);
    this.anchor.setTo(0.5, 0.5);

    this.y = this.game.height / 2 - 200;
    this.game.physics.arcade.enableBody(this);

    this.body.allowGravity = false;
    this.body.immovable = true;

    this.body.velocity.x = -150;


};

WallArt.prototype = Object.create(Phaser.Sprite.prototype);
WallArt.prototype.constructor = WallArt;

WallArt.prototype.update = function() {
    // write your prefab's specific update code here


};

module.exports = WallArt;
