'use strict';

var Message = require('./message');

var Messages = function(game, parent) {
    Phaser.Group.call(this, game, parent);

    this.messageBeaverPower = new Message(this.game, this.game.world.centerX, this.game.world.centerY, 'messageBeaverPower');
    this.add(this.messageBeaverPower);

    this.messageHitFly = new Message(this.game, this.game.world.centerX, this.game.world.centerY, 'messageHitFly');
    this.add(this.messageHitFly);

    this.messageHitRat = new Message(this.game, this.game.world.centerX, this.game.world.centerY, 'messageHitRat');
    this.add(this.messageHitRat);

    this.messageRefill = new Message(this.game, this.game.world.centerX, this.game.world.centerY, 'messageRefill');
    this.add(this.messageRefill);

    this.messageBeaverPower.cacheAsBitmap = true;
    this.messageHitFly.cacheAsBitmap = true;
    this.messageHitRat.cacheAsBitmap = true;
    this.messageRefill.cacheAsBitmap = true;
};

Messages.prototype = Object.create(Phaser.Group.prototype);
Messages.prototype.constructor = Messages;

Messages.prototype.update = function() {
    // write your prefab's specific update code here

};

Messages.prototype.play = function(id) {
    this['message' + id].play();
}

module.exports = Messages;
