'use strict';

var ShotGlassObstacle = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'shotGlass', frame);
    this.anchor.setTo(0.5, 0.5);
    this.game.physics.arcade.enableBody(this);
    this.animations.frame = 0;
    this.body.allowGravity = false;
    this.body.immovable = true;

};

ShotGlassObstacle.prototype = Object.create(Phaser.Sprite.prototype);
ShotGlassObstacle.prototype.constructor = ShotGlassObstacle;



ShotGlassObstacle.prototype.update = function() {
    // write your prefab's specific update code here

};

ShotGlassObstacle.prototype.hit = function() {


}

module.exports = ShotGlassObstacle;
