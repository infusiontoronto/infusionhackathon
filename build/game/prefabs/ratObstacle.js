'use strict';

var RatObstacle = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'obstacle', frame);
    this.anchor.setTo(0.5, 0.5);
    this.game.physics.arcade.enableBody(this);

    this.body.allowGravity = false;
    this.body.immovable = true;

};

RatObstacle.prototype = Object.create(Phaser.Sprite.prototype);
RatObstacle.prototype.constructor = RatObstacle;

RatObstacle.prototype.update = function() {
    // write your prefab's specific update code here

};

RatObstacle.prototype.hit = function() {
    this.reset();
}

module.exports = RatObstacle;
