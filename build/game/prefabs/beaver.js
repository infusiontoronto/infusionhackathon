'use strict';
var beaverSplitEmitter;
var justHit = false;
var ready = false;

var Beaver = function(game, x, y, frame) {
    Phaser.Sprite.call(this, game, x, y, 'beaver', frame);

    // set the sprite's anchor to the center
    this.anchor.setTo(0.5, 0.5);

    // add and play animations
    this.animations.add('run');
    this.animations.play('run', 2, true);
    // this.animations.stop();

    this.game.physics.arcade.enableBody(this);
    this.body.velocity.x = 50;

    beaverSplitEmitter = this.game.add.emitter(this.game.world.centerX + 100, 330, 4000);
    beaverSplitEmitter.makeParticles(['beerParticle1', 'beerParticle2', 'beerParticle3', 'beerParticle4']);
    beaverSplitEmitter.gravity = 400;
    beaverSplitEmitter.width = 60;
    beaverSplitEmitter.setAlpha(1, 0, 3000);
    beaverSplitEmitter.setScale(0.2, 0.1, 0.2, 0.1, 3000);

};

Beaver.prototype = Object.create(Phaser.Sprite.prototype);
Beaver.prototype.constructor = Beaver;

Beaver.prototype.update = function() {
    // justHit = true;
    if (ready) {
        // this.body.x = this.game.world.centerX + 100;
        this.body.velocity.x += 5;
    }

};

Beaver.prototype.hit = function() {
    beaverSplitEmitter.y = this.body.y;
    beaverSplitEmitter.x = this.body.x + 50;
    beaverSplitEmitter.start(true, 1000, null, 15);
}


Beaver.prototype.spawn = function() {

    var destX = this.game.world.centerX + 200;
    this.body.x = -100;
    var t = this.game.add.tween(this).to({
        x: destX
    }, 1000, Phaser.Easing.Quadratic.Out);
    t.onComplete.addOnce(this.ready, this);
    t.start();
}

Beaver.prototype.ready = function() {
    ready = true;
}
Beaver.prototype.done = function() {
    ready = false;
}
module.exports = Beaver;
