'use strict';
var justHit = false;
var scrollSpeed = -200;
var BarTop = function(game, x, y, width, height) {
    Phaser.TileSprite.call(this, game, x, y, width, height, 'ground');

    this.autoScroll(scrollSpeed, 0);

    // initialize your prefab here
    this.game.physics.arcade.enableBody(this);

    this.body.allowGravity = false;
    this.body.immovable = true;
};

BarTop.prototype = Object.create(Phaser.TileSprite.prototype);
BarTop.prototype.constructor = BarTop;

BarTop.prototype.update = function() {

    // write your prefab's specific update code here
    if (justHit) {

        if (this.scrollSpeed > this.game.scrollSpeed) {
            this.scrollSpeed -= 5;
            this.autoScroll(this.scrollSpeed, 0);
        } else {
            this.scrollSpeed = this.game.scrollSpeed;
            this.autoScroll(this.scrollSpeed, 0);
            justHit = false;
        }
    }
};

BarTop.prototype.stopMoving = function() {
    this.autoScroll(0, 0);
}


BarTop.prototype.hit = function() {
    this.scrollSpeed = 0;
    this.stopMoving();
    justHit = true;
}


BarTop.prototype.startMoving = function() {
    this.autoScroll(scrollSpeed, 0);
}

module.exports = BarTop;
