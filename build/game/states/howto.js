'use strict';

function Howto() {}
Howto.prototype = {
    preload: function() {
        // Override this method to add some load operations. 
        // If you need to use the loader, you may need to use them here.
    },
    create: function() {
        // This method is called after the game engine successfully switches states. 
        // Feel free to add any setup code here (do not load anything here, override preload() instead).
        //this.screenImage = this.game.add.sprite(0,0,'howto-scoreboard-bg');

        this.howToGroup = this.game.add.group();
        this.bg = this.game.add.sprite(0, 0, 'howto-bg');
        this.howToGroup.add(this.bg);

        this.backButton = this.game.add.button(50, this.game.height - 180, 'btn-back', this.backButtonClick, this);
        this.howToGroup.add(this.backButton);
    },
    update: function() {
        // state update code
        if (this.game.input.activePointer.justPressed()) {
            this.game.state.start('menu');
        }
    },
    paused: function() {
        // This method will be called when game paused.
    },
    render: function() {
        // Put render operations here.
    },
    shutdown: function() {
        // This method will be called when the state is shut down 
        // (i.e. you switch to another state from this one).
    }
};
module.exports = Howto;
