'use strict';

var _this;

function GameOver() {}

GameOver.prototype = {
    preload: function() {

    },
    create: function() {
        _this = this;

        this.game.stage.filters = null;

        var data = new Image();
        data.src = this.game.lastStateBmd;
        this.game.cache.addImage('image-data', this.game.lastStateBmd, data);
        this.game.add.sprite(0, 0, 'image-data');

        this.titleText = this.game.add.sprite(this.game.world.centerX, 340, 'gameOver');
        this.titleText.anchor.setTo(0.5, 0);
        this.titleText.alpha = 0;

        var t1 = this.game.add.tween(this.titleText).to({
            alpha: 1,
            y: 190
        }, 1000, Phaser.Easing.Back.Out, true, 0, 0, false);

        var style = {
            font: '80px Arial',
            fill: '#ffffff',
            align: 'center'
        };
        var message = 'SCORE: ' + this.game.score + '        LEVEL: ' + this.game.level;
        this.titleScore = this.game.add.text(this.game.world.centerX, 490, message, style);
        this.titleScore.anchor.setTo(0.5, 0);
        this.titleScore.alpha = 0;

        var t2 = this.game.add.tween(this.titleScore).to({
            alpha: 1,
            y: 440
        }, 750, Phaser.Easing.Back.Out, true, 250, 0, false);

        var style = {
            font: '80px Arial',
            fill: '#ff9a00',
            fontWeight: 'bold',
            align: 'center'
        };
        this.titleSave = this.game.add.text(this.game.world.centerX, 755, 'SAVE TO LEADERBOARD', style);
        this.titleSave.anchor.setTo(0.5, 0);
        this.titleSave.alpha = 0;

        this.titleSave.inputEnabled = true;
        this.titleSave.events.onInputDown.add(this.showInput, this);

        var t3 = this.game.add.tween(this.titleSave).to({
            alpha: 1,
            y: 705
        }, 750, Phaser.Easing.Back.Out, true, 500, 0, false);

        this.titleShare = this.game.add.text(this.game.world.centerX - 105, 890, 'SHARE', style);
        this.titleShare.anchor.setTo(0.5, 0);
        this.titleShare.alpha = 0;

        var t4 = this.game.add.tween(this.titleShare).to({
            alpha: 1,
            y: 840
        }, 1000, Phaser.Easing.Back.Out, true, 750, 0, false);

        this.shareTwitter = this.game.add.button(this.titleShare.x + this.titleShare.width * 0.5 + 80, this.titleShare.y + 5, 'btn-share-twitter-small', this.shareTwitterClick, this);
        this.shareTwitter.anchor.setTo(0.5);
        this.shareTwitter.alpha = 0;

        var t5 = this.game.add.tween(this.shareTwitter).to({
            alpha: 1,
            y: this.shareTwitter.y - 10
        }, 500, Phaser.Easing.Back.Out, true, 850, 0, false);

        this.shareFacebook = this.game.add.button(this.shareTwitter.x + this.shareTwitter.width + 40, this.shareTwitter.y, 'btn-share-facebook-small', this.shareFacbookClick, this);
        this.shareFacebook.anchor.setTo(0.5);
        this.shareFacebook.alpha = 0;

        var t6 = this.game.add.tween(this.shareFacebook).to({
            alpha: 1,
            y: this.shareFacebook.y - 10
        }, 500, Phaser.Easing.Back.Out, true, 950, 0, false);

        this.restartGame = this.game.add.button(45, 920, 'btn-restart', this.restartGame, this);
        this.restartGame.alpha = 0;

        var t7 = this.game.add.tween(this.restartGame).to({
            alpha: 1,
            y: this.restartGame.y - 10
        }, 1000, Phaser.Easing.Back.Out, true, 1250, 0, false);
    },
    update: function() {
        // if(this.game.input.activePointer.justPressed()) {
        //   this.game.state.start('play');
        // }
    },
    shareTwitterClick: function() {
        //share on twitter
        var tweetbegin = 'http://twitter.com/home?status=';
        var tweettxt = 'I like this game Beer Run -' + window.location.href + '.';
        var finaltweet = tweetbegin + encodeURIComponent(tweettxt);
        window.open(finaltweet, '_blank');
    },
    shareFacbookClick: function() {
        // var shareLink = 'https://www.facebook.com/sharer/sharer.php?u=http://infusiondevelop.com'
        var shareLink = "http://www.facebook.com/sharer.php?s=100&amp;p[title]=Beer Run!&amp;p[summary]=Check out Beer Run by the Interactive Development team at Infusion Toronto&amp;p[url]=www.infusion.com&amp;p[images][0]=http://infusion.com/Content/css/logo.png"
        window.open(shareLink, '_blank');
    },
    showInput: function() {
        this.titleSave.alpha = 0;

        var inputHTML = '<div id="inputContainer">' +
            '  <input type="text" id="nameInput" placeholder="Enter your name">' +
            '  <button type="button" id="submitBtn">Submit</button>' +
            '</div>' +
            '<script>';
        if (document.getElementById("inputContainer") != null) {
            document.getElementById("inputContainer").parentNode.remove(document.getElementById("inputContainer"));
        }

        document.querySelectorAll('canvas')[0].insertAdjacentHTML('afterend', inputHTML);
        document.querySelectorAll('#submitBtn')[0].addEventListener('mousedown', this.submitScore);

        var ratio = (parseInt(document.getElementsByTagName("canvas")[0].style.width) / 1920);
        document.getElementById("inputContainer").style.zoom = ratio;
        document.getElementById("inputContainer").style["margin-left"] = document.getElementsByTagName("canvas")[0].style["margin-left"];
        document.getElementById("inputContainer").style["-moz-transform"] = "scale(" + ratio + ")";

    },
    submitScore: function() {
        var userName = document.getElementById("nameInput").value;
        var data = "one=" + userName + "&two=" + _this.game.score + "&three=" + _this.game.level;

        document.getElementById("inputContainer").remove();

        var request = new XMLHttpRequest();
        request.open('GET', 'http://beerrun.co/game/hackathon/set_score.php?' + data, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // here you could go to the leaderboard or restart your game

                var obj = JSON.parse(request.responseText);


            } else {
                // We reached our target server, but it returned an error

            }
        };
        request.send(data);
    },
    restartGame: function() {
        location.reload();
    }
};
module.exports = GameOver;
