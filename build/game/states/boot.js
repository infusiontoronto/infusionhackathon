'use strict';

function Boot() {}

Boot.prototype = {
    preload: function() {
        this.load.image('preloader', 'assets/preloader' + this.game.suffix + '.gif');
        this.load.image('loading', 'assets/loading-screen-' + this.game.suffix + '.jpg');
    },
    create: function() {

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.setShowAll();
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVeritcally = true;
        this.scale.refresh();

        this.game.input.maxPointers = 1;
        this.game.state.start('preload');
    }
};

module.exports = Boot;
