'use strict';
var scoreData, levelData;
var objectCollection = new Array();

function Leaderboard() {}
Leaderboard.prototype = {
    preload: function() {
        // Override this method to add some load operations. 
        // If you need to use the loader, you may need to use them here.


    },
    create: function() {
        // This method is called after the game engine successfully switches states. 
        // Feel free to add any setup code here (do not load anything here, override preload() instead).
        var ref = this;

        var request = new XMLHttpRequest();
        request.open('GET', 'http://beerrun.co/game/hackathon/get_scores.php', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                // here you could go to the leaderboard or restart your game

                var leaderBoardData = JSON.parse(request.responseText);
                scoreData = leaderBoardData[0];
                levelData = leaderBoardData[1];

                ref.createUI();

                ref.showScoreData();

            } else {
                // We reached our target server, but it returned an error
                alert("ERROR LOADING SCORES, RETURNING TO THE HOME SCREEN");
                this.game.state.start('menu');
            }
        };
        request.send();

    },
    createUI: function() {

        this.titleGroup = this.game.add.group();

        this.bg = this.game.add.sprite(0, 0, 'submenu-bg');
        this.titleGroup.add(this.bg);

        this.title = this.game.add.sprite(0, 0, 'leader-title');
        this.title.anchor.setTo(0.5, 0.5);
        this.title.x = this.game.width / 2;
        this.title.y = 100;
        this.titleGroup.add(this.title);
        // button = game.add.button(game.world.centerX - 95, 400, 'button', actionOnClick, this, 2, 1, 0);

        this.scoreButton = this.game.add.button(422, 795, 'topScore', this.showScoreData, this);
        // this.scoreButton.anchor.setTo(0.5,0.5);
        this.titleGroup.add(this.scoreButton);
        this.scoreButton.outFrame = this.scoreButton.overFrame = this.scoreButton.downFrame = this.scoreButton.upFrame = 1;
        this.scoreButton.setFrames(0, 0, 0, 0);

        this.levelButton = this.game.add.button(1040, 795, 'topLevel', this.showLevelData, this);
        // this.levelButton.anchor.setTo(0.5,0.5);
        this.titleGroup.add(this.levelButton);
        // this.levelButton.outFrame= this.levelButton.overFrame= this.levelButton.downFrame = this.levelButton.upFrame = 0;
        this.levelButton.setFrames(1, 1, 1, 1);
        this.scoreGroup = this.game.add.group();

        this.backButton = this.game.add.button(50, this.game.height - 180, 'btn-back', this.shutdown, this);
        this.scoreGroup.add(this.backButton);
    },
    showScoreData: function() {

        this.clearData();

        var nameStyle = {
            font: "60px Arial",
            fill: "#FFFFFF",
            align: "left",
            weight: "bold"
        };
        var generalStyle = {
            font: "60px Arial",
            fill: "#FFFFFF",
            align: "left"
        };

        var num = 0;

        for (var prop in scoreData) {
            num++;

            // important check that this is objects own property 
            // not from prototype prop inherited
            if (scoreData.hasOwnProperty(prop) && num < 6) {

                var textName = this.game.add.text(315, 150 + (85 * num), scoreData[prop].name, nameStyle);
                var textScore = this.game.add.text(880, 150 + (85 * num), scoreData[prop].score, generalStyle);
                var textLevel = this.game.add.text(1365, 150 + (85 * num), "LEVEL " + scoreData[prop].level, generalStyle);
                objectCollection.push(textName);
                objectCollection.push(textLevel);
                objectCollection.push(textScore);
                this.scoreGroup.add(textName);
                this.scoreGroup.add(textLevel);
                this.scoreGroup.add(textScore);
            }

        }

        this.scoreButton.setFrames(0, 0, 0, 0);
        this.levelButton.setFrames(1, 1, 1, 1);

    },
    showLevelData: function() {

        this.clearData();

        var nameStyle = {
            font: "60px Arial",
            fill: "#FFFFFF",
            align: "left",
            weight: "bold"
        };
        var generalStyle = {
            font: "60px Arial",
            fill: "#FFFFFF",
            align: "left"
        };

        var num = 0;

        for (var prop in levelData) {
            num++;
            // important check that this is objects own property 
            // not from prototype prop inherited
            if (levelData.hasOwnProperty(prop) && num < 6) {

                var textName = this.game.add.text(315, 150 + (85 * num), levelData[prop].name, nameStyle);
                var textLevel = this.game.add.text(840, 150 + (85 * num), "LEVEL " + levelData[prop].level, generalStyle);
                var textScore = this.game.add.text(1405, 150 + (85 * num), levelData[prop].score, generalStyle);
                objectCollection.push(textName);
                objectCollection.push(textLevel);
                objectCollection.push(textScore);
                this.scoreGroup.add(textName);
                this.scoreGroup.add(textLevel);
                this.scoreGroup.add(textScore);
            }
        }
        this.levelButton.setFrames(0, 0, 0, 0);
        this.scoreButton.setFrames(1, 1, 1, 1);
    },
    clearData: function() {

        for (var i = objectCollection.length - 1; i >= 0; i--) {
            var text = objectCollection.splice(i, 1);
            text[0].destroy();
        }
        // this.game.remove.group(scoreGroup);
    },
    update: function() {
        // state update code
    },
    paused: function() {
        // This method will be called when game paused.
    },
    render: function() {
        // Put render operations here.
    },
    shutdown: function() {
        this.clearData();
        this.game.state.start('menu');

        // This method will be called when the state is shut down 
        // (i.e. you switch to another state from this one).
    }
};
module.exports = Leaderboard;
