'use strict';
var Beer = require('../prefabs/beer');
var Beaver = require('../prefabs/beaver');
var Ground = require('../prefabs/barTop');
var BackwallGroup = require('../prefabs/backwallGroup');
var RatObstacleGroup = require('../prefabs/ratObstacleGroup');
var ShotGlassGroup = require('../prefabs/shotGlassGroup');
var BarflyObstacleGroup = require('../prefabs/barflyObstacleGroup');
var BeerTaps = require('../prefabs/beerTaps.js');
var HUD = require('../prefabs/HUD');
var MessagesGroup = require('../prefabs/messagesGroup.js');
var BlurX = require('../filters/BlurX');
var BlurX = require('../filters/BlurY');

var _this;
var curLevel = 0;
var curBeerAmount = 100;
var refillAmount = 25;
var refillAllowed = false;
var beerDrain = 0.02;

var beaverActive = false;

var filterX;
var filterY;
var blurring = false;
var drunkTweenX;
var drunkTweenY;
var renderTexture;
var renderTexture2;

var outputSprite;

function Play() {}


Play.prototype = {
    create: function() {
        _this = this;

        renderTexture = this.game.add.renderTexture(this.game.width, this.game.height, 'texture1');
        renderTexture2 = this.game.add.renderTexture(this.game.width, this.game.height, 'texture1');
        outputSprite = this.game.add.sprite(this.game.centerX, this.game.centerY, renderTexture);
        outputSprite.anchor.x = 0.5;
        outputSprite.anchor.y = 0.5;

        // start the phaser arcade physics engine
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 1200;

        curLevel = 1;
        this.game.scrollSpeed = -200;
        this.backwall = this.game.add.group();

        this.bg = new BackwallGroup(this.game, this.backwall);

        // create and add a new Ground object
        this.ground = new Ground(this.game, 0, this.game.height - 350, 1920, 350);
        this.ground.body.setSize(1920, 350, 0, 150);
        this.game.add.existing(this.ground);

        // create and add beer taps
        this.beerTaps = new BeerTaps(this.game, this.game.world.width, this.game.world.centerY);
        this.game.add.existing(this.beerTaps);
        this.beerTaps.cacheAsBitmap = true;

        // create and add a new Beer object
        this.beer = new Beer(this.game, -100, this.game.height / 3);
        this.game.add.existing(this.beer);
        this.beer.spawn();

        // create and add a group to hold our pipeGroup prefabs
        this.obstacles = this.game.add.group();
        this.shots = this.game.add.group();

        this.addListeners();

        // add a timer
        this.obstacleGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 1.5, this.generateObstacles, this);
        this.obstacleGenerator.timer.start();

        this.powerupGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 5.5, this.generatePowerups, this);
        this.powerupGenerator.timer.start();

        this.beerTapGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 3, this.generateBeerTaps, this);
        this.beerTapGenerator.timer.start();

        this.levelIncrementer = this.game.time.events.loop(Phaser.Timer.SECOND * 20, this.incrementLevel, this);
        this.levelIncrementer.timer.start();

        filterX = this.game.add.filter('BlurX', this.game.width, this.game.height);
        filterX.blur = 0;
        filterY = this.game.add.filter('BlurY', this.game.width, this.game.height);
        filterY.blur = 0;

        this.game.stage.filters = [filterX, filterY];

        this.hud = new HUD(this.game, undefined);
        this.score = 0;
        _this.score = 0;
        this.game.add.existing(this.hud);

        this.messages = this.game.add.group();
        this.messagesGroup = new MessagesGroup(this.game, this.messages);
    },

    addListeners: function() {

        // add keyboard controls
        this.jumpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.jumpKey.onDown.add(this.beer.jump, this.beer);

        // add mouse/touch controls
        this.game.input.onDown.add(this.beer.jump, this.beer);

        this.beerTaps.inputEnabled = true;
        this.beerTaps.events.onInputDown.add(this.refillDrink, this);

    },
    refillDrink: function() {
        if (refillAllowed) {
            curBeerAmount = Math.min(curBeerAmount + refillAmount, 100);
            this.beer.updateFillAmount(curBeerAmount);
            refillAllowed = false;
            this.messagesGroup.play('Refill');
        }
    },
    resetDrunk: function() {

        // var bmd = this.game.add.bitmapData(1920, 1080);
        // var area = new Phaser.Rectangle(0, 0, 1920, 1080);

        // bmd.copyPixels(this.game.stage, area, 0, 0);

        // this.game.add.sprite(0,0,bmd)    

        curBeerAmount = 100;
        this.beer.updateFillAmount(curBeerAmount);
        this.beer.refill();
    },
    update: function() {
        if (this.beer.body.y > this.game.height) this.deathHandler();

        if (this.hud.isPaused() == true) {
            this.pause();
        }

        if (beaverActive) {

            this.game.physics.arcade.collide(this.beaver, this.ground, null, null, this);

            this.obstacles.forEach(function(obstacleGroup) {
                this.game.physics.arcade.collide(this.beaver, obstacleGroup, this.beaverHitHandler, null, obstacleGroup);
            }, this);

            this.shots.forEach(function(obstacleGroup) {
                this.game.physics.arcade.collide(this.beaver, obstacleGroup, this.beaverHitHandler, null, obstacleGroup);
            }, this);


        }

        // enable collisions between the birbeerd and the ground
        if (curBeerAmount <= 0) {
            this.deathHandler();
            return;
        }

        this.game.physics.arcade.collide(this.beer, this.ground, this.groundHit, null, this.beer);

        // enable collisions between the beer and each group in the rats group
        this.obstacles.forEach(function(obstacleGroup) {
            this.game.physics.arcade.collide(this.beer, obstacleGroup, this.obstacleHitHandler, null, obstacleGroup);
        }, this);


        this.shots.forEach(function(obstacleGroup) {
            this.game.physics.arcade.collide(this.beer, obstacleGroup, this.shotHitHandler, null, obstacleGroup);
        }, this);


        if (!this.beerTaps.inWorld) {
            this.beerTaps.reset();
        }

        curBeerAmount -= beerDrain;
        this.hud.updateLevel(curLevel);

        this.beer.updateFillAmount(curBeerAmount);

        if (!this.game.paused) {

            if (filterX != null && curBeerAmount < 90 && !blurring) {

                var shouldBlur = this.game.rnd.integerInRange(0, 1000);
                var blurMe = false;

                if (curBeerAmount < 50) {
                    if (shouldBlur % 20 == 0) {
                        blurMe = true;
                    }
                } else {
                    if (shouldBlur % 100 == 0) {
                        blurMe = true;
                    }
                }

                if (blurMe) {
                    var filterAmount = 200 - ((curBeerAmount / 100) * 200);

                    blurring = true;

                    drunkTweenX = this.game.add.tween(filterX).to({
                        blur: filterAmount
                    }, 2000, Phaser.Easing.Quadratic.InOut, false, 0, 1, true);
                    drunkTweenY = this.game.add.tween(filterY).to({
                        blur: filterAmount / 4
                    }, 2000, Phaser.Easing.Quadratic.InOut, true, 0, 1, true);

                    drunkTweenX.onComplete.addOnce(this.endDrunkTween, this);
                    drunkTweenX.start();
                }


            } else if (filterX != null && curBeerAmount >= 90 && !blurring) {
                filterX.blur = 0;
                filterY.blur = 0;
            }
        } else {
            filterX.blur = 0;
            filterY.blur = 0;

        }

        filterX.update();
        filterY.update();

        if (this.emitter != null) {
            var px = 4;
            var py = 1;

            px *= -1;
            py *= -1;

            this.emitter.minParticleSpeed.set(px, py);
            this.emitter.maxParticleSpeed.set(px, py);

            this.emitter.emitX = this.game.width / 2;
            this.emitter.emitY = this.game.height / 2;

        }

        // var temp = renderTexture;
        // renderTexture = renderTexture2;
        // renderTexture2 = temp;

        // outputSprite.setTexture(renderTexture);

        // outputSprite.scale.x = outputSprite.scale.y  = 1 + Math.sin(1) * 0.2;


        // renderTexture2.renderXY(this.game.stage, 0, 0, true);
    },
    groundHit: function() {

        if (this.jumped) {
            this.jumped = false;
            if (Math.abs(this.angle) >  50) {
                
                this.y -= 250;
                this.x -= 150;
                this.angle = 0;
                this.hit();

                curBeerAmount -= 5;
                _this.hud.updateBeerAmount(Math.round(curBeerAmount));
                _this.hud.updateScore(_this.score);

            }
        }
        
    },
    endDrunkTween: function() {

        blurring = false;
        drunkTweenX.stop();
        drunkTweenY.stop();
    },
    paused: function() {
        // This method will be called when game paused.

    },
    unpause: function() {
        this.game.input.onDown.remove(this.unpause, this);
        this.menu.destroy();
        this.choiseLabel.destroy();
        this.game.paused = false;
        this.hud.unpause();

    },
    pause: function() {

        if (this.game.paused == true) return;

        this.game.input.onDown.add(this.unpause, this);
        this.game.paused = true;

        this.pauseGroup = this.game.add.group();

        this.menu = this.game.add.sprite(0, 0, 'pause-bg');
        // this.menu.anchor.setTo(0.5, 0.5);

        this.pauseGroup.add(this.menu);

        this.choiseLabel = this.game.add.text(this.menu.width / 2, this.menu.height / 2, 'Click anywhere menu to go back to the game', {
            font: '30px Arial',
            fill: '#fff'
        });
        this.choiseLabel.anchor.setTo(0.5, 0.5);
        this.pauseGroup.add(this.choiseLabel);

        var tempW = this.menu.width;
        this.menu.width = this.game.width;
        var scaleVal = this.menu.scale.x;
        this.menu.width = tempW;
        this.pauseGroup.scale.setTo(scaleVal, scaleVal);


        return;

        // TODO, get a particle system to look like pee and play while the game is paused


        this.emitter = this.game.add.emitter(this.game.world.centerX / 2, this.game.height, 2000);

        this.emitter.makeParticles(['fire1', 'fire2', 'fire3']);

        // this.emitter.gravity = -200;
        this.emitter.setAlpha(1, 0, 3000);
        this.emitter.setScale(0.8, 0, 0.8, 0, 3000);

        this.emitter.particleBringToTop = true;

        this.emitter.start(false, 3000, 5);


    },
    beaverHitHandler: function() {
        this.hit();
        _this.beaver.hit();
        _this.score += 100;
        this.score += 100;
        curBeerAmount += 0.01;
        _this.hud.updateBeerAmount(Math.round(curBeerAmount));
        _this.hud.updateScore(_this.score);

    },
    obstacleHitHandler: function(e) {
        this.hit();
        curBeerAmount -= this.obstacleDamage();

        _this.hud.updateBeerAmount(curBeerAmount);
        _this.beer.hit();

        _this.messagesGroup.play(this.hitMessage());
    },
    shotHitHandler: function() {
        this.hit();
        _this.beer.hit();

        curBeerAmount += 1;

        _this.activateBeaver();

        // this.game.pause();
    },
    activateBeaver: function() {
        if (beaverActive) return;

        beaverActive = true;
        this.beaver = new Beaver(this.game, -100, this.game.height / 4);
        this.game.add.existing(this.beaver);

        this.beaver.spawn();

        this.messagesGroup.play('BeaverPower');

        setTimeout(this.killBeaver, 3500);
    },
    killBeaver: function() {
        _this.beaver.done();
        
        var t = _this.game.add.tween(_this.beaver).to({
            x: 2000
        }, 1000, Phaser.Easing.Quadratic.Out);

        t.onComplete.addOnce(_this.destroyBeaver, _this);
        t.start();

    },
    destroyBeaver: function() {
        this.beaver.destroy();
        beaverActive = false;
    },
    shutdown: function() {
        // this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        // this.beer.destroy();
        // this.obstacles.destroy();
    },
    deathHandler: function() {
        this.game.level = curLevel;
        this.game.score = this.score;

        this.game.lastStateBmd = this.game.canvas.toDataURL();
        this.game.state.start('gameover');
    },
    incrementLevel: function() {
        curLevel++;
        this.game.scrollSpeed = -200 - (curLevel * 10);
    },
    reset: function() {
        this.score = 0;
        curBeerAmount = 100;
        curLevel = 1;
        this.game.scrollSpeed = -200;
    },
    generateObstacles: function() {
        // make shit a little more random
        if ((this.game.rnd.integerInRange(0, 99) % 2 == 0)) return;

        var offsetY = this.game.rnd.integerInRange(-30, 0);

        var isRat = (this.game.rnd.integerInRange(0, 99) % 2 == 0);
        var obstacleGroup = this.obstacles.getFirstExists(false);
        
        if (isRat) {
            if (!obstacleGroup) {
                obstacleGroup = new RatObstacleGroup(this.game, this.obstacles);
            }
        } else {
            if (!obstacleGroup) {
                obstacleGroup = new BarflyObstacleGroup(this.game, this.obstacles);
            }
        }

        obstacleGroup.reset(this.game.width, offsetY);

    },
    generatePowerups: function() {
        // make shit a little more random
        if ((this.game.rnd.integerInRange(0, 99) % 2 == 0)) return;

        var offsetY = this.game.rnd.integerInRange(-30, 0);

        var obstacleGroup = this.shots.getFirstExists(false);

        if (!obstacleGroup) {
            obstacleGroup = new ShotGlassGroup(this.game, this.shots);
        }

        obstacleGroup.reset(this.game.width, offsetY);

    },
    generateBeerTaps: function() {
        if (curBeerAmount < 80) {
            var shouldTapBeer = this.game.rnd.integerInRange(0, 1000);
            if (shouldTapBeer % 10 == 0) {
                refillAllowed = true;
                this.beerTaps.show();
            }
        }
    }
};

module.exports = Play;
