'use strict';

function Menu() {}

Menu.prototype = {
    preload: function() {},
    create: function() {
        // this.background = this.game.add.sprite(0, 0, 'title-bg');

        this.titleGroup = this.game.add.group();

        this.bg = this.game.add.sprite(0, 0, 'title-bg');
        this.titleGroup.add(this.bg);

        // this.title = this.game.add.sprite(0,0,'title-logo');
        // this.titleGroup.add(this.title);

        this.startButton = this.game.add.button(830, 500, 'menu-play', this.startClick, this);
        this.startButton.anchor.setTo(0.5, 0.5);
        this.titleGroup.add(this.startButton);

        this.leaderboardButton = this.game.add.button(875, 625, 'menu-leader', this.leaderboardClick, this);
        this.leaderboardButton.anchor.setTo(0.5, 0.5);
        this.titleGroup.add(this.leaderboardButton);

        this.howtoButton = this.game.add.button(900, 750, 'menu-howto', this.howtoClick, this);
        this.howtoButton.anchor.setTo(0.5, 0.5);
        this.titleGroup.add(this.howtoButton);

        this.shareButton = this.game.add.button(925, 875, 'menu-share', this.shareClick, this);
        this.shareButton.anchor.setTo(0.5, 0.5);
        this.titleGroup.add(this.shareButton);

        this.fsButton = this.game.add.button(50, this.game.height - 120 - 50, 'fs-btn', this.fullscreen, this);
        this.titleGroup.add(this.fsButton);

        // var tempW = this.bg.width;
        // this.bg.width = this.game.width;
        // var scaleVal = this.bg.scale.x;
        // this.bg.width = tempW;
        // this.titleGroup.scale.setTo(scaleVal, scaleVal);

    },
    fullscreen: function() {
        this.game.scale.startFullScreen();
    },
    startClick: function() {
        // start button click handler
        // start the 'play' state
        this.game.state.start('play');
    },
    leaderboardClick: function() {
        // start button click handler
        // start the 'play' state
        this.game.state.start('leaderboard');
    },
    howtoClick: function() {
        // start button click handler
        // start the 'play' state
        this.game.state.start('howto');
    },
    shareClick: function() {
        // start button click handler
        // start the 'play' state
        this.game.state.start('share');
    },
    update: function() {}
};
module.exports = Menu;
