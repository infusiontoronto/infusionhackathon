'use strict';

function Preload() {
    this.asset = null;
    this.ready = false;
}


Preload.prototype = {
    preload: function() {

        this.background = this.game.add.image(0, 0, 'loading');

        this.asset = this.add.sprite(this.width / 2, this.height / 2, 'preloader');
        this.asset.anchor.setTo(0.5, 0.5);

        this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
        this.load.setPreloadSprite(this.asset);

        this.load.image('background', 'assets/background/backwall-tile-' + this.game.suffix + '.jpg');
        this.load.image('background-art-1', 'assets/background/backwall-art-1-' + this.game.suffix + '.png');
        this.load.image('background-art-2', 'assets/background/backwall-art-2-' + this.game.suffix + '.png');
        this.load.image('background-art-3', 'assets/background/backwall-art-3-' + this.game.suffix + '.png');
        this.load.image('background-art-4', 'assets/background/backwall-art-4-' + this.game.suffix + '.png');
        this.load.image('background-art-5', 'assets/background/backwall-art-5-' + this.game.suffix + '.png');

        this.load.image('background', 'assets/background-' + this.game.suffix + '.jpg');
        this.load.image('howto-bg', 'assets/howto-bg-' + this.game.suffix + '.jpg');
        this.load.image('fs-btn', 'assets/btn-fullscreen-' + this.game.suffix + '.png');
        this.load.image('howto-scoreboard-bg', 'assets/scoreboard-howto-bg-' + this.game.suffix + '.jpg');
        this.load.image('ground', 'assets/bartop-' + this.game.suffix + '.jpg');
        this.load.image('title', 'assets/title-' + this.game.suffix + '.png');
        this.load.spritesheet('beer', 'assets/pint-' + this.game.suffix + '.png', 96, 168, 6);
        this.load.spritesheet('beaver', 'assets/beaver-' + this.game.suffix + '.png', 206, 102, 2);
        this.load.image('obstacle', 'assets/rat-' + this.game.suffix + '.png');
        this.load.image('beerTaps', 'assets/beerTaps-' + this.game.suffix + '.png');
        this.load.image('fries', 'assets/fries-' + this.game.suffix + '.png');
        this.load.spritesheet('shotGlass', 'assets/shotGlass-' + this.game.suffix + '.png', 126, 61);
        this.load.image('barfly', 'assets/barfly-' + this.game.suffix + '.png');


        this.load.image('title-logo', 'assets/menu_logo-' + this.game.suffix + '.png');
        this.load.image('title-bg', 'assets/menu_bg-' + this.game.suffix + '.jpg');
        this.load.image('HUD', 'assets/hud_bg-' + this.game.suffix + '.png');

        this.load.image('pause-bg', 'assets/pause-' + this.game.suffix + '.jpg');

        this.load.spritesheet('topScore', 'assets/button_score_tab-' + this.game.suffix + '.png', 478, 45);
        this.load.spritesheet('topLevel', 'assets/button_level_tab-' + this.game.suffix + '.png', 459, 45);

        this.load.image('submenu-bg', 'assets/submenu-bg-' + this.game.suffix + '.jpg');
        this.load.image('leader-title', 'assets/leader-title-' + this.game.suffix + '.png');

        this.load.image('fire1', 'assets/fire1-' + this.game.suffix + '.png');
        this.load.image('fire2', 'assets/fire2-' + this.game.suffix + '.png');
        this.load.image('fire3', 'assets/fire3-' + this.game.suffix + '.png');
        this.load.image('smoke', 'assets/smoke-puff-' + this.game.suffix + '.png');

        this.load.image('menu-play', 'assets/menu_btn_play-' + this.game.suffix + '.png');
        this.load.image('menu-leader', 'assets/menu_btn_leaderboard-' + this.game.suffix + '.png');
        this.load.image('menu-howto', 'assets/menu_btn_howto-' + this.game.suffix + '.png');
        this.load.image('menu-share', 'assets/menu_btn_share-' + this.game.suffix + '.png');

        this.load.image('beerParticle1', 'assets/beer_particle_1-' + this.game.suffix + '.png');
        this.load.image('beerParticle2', 'assets/beer_particle_2-' + this.game.suffix + '.png');
        this.load.image('beerParticle3', 'assets/beer_particle_3-' + this.game.suffix + '.png');
        this.load.image('beerParticle4', 'assets/beer_particle_4-' + this.game.suffix + '.png');

        this.load.image('messageBeaverPower', 'assets/msg-beaver-power-' + this.game.suffix + '.png');
        this.load.image('messageHitFly', 'assets/msg-hit-fly-' + this.game.suffix + '.png');
        this.load.image('messageHitRat', 'assets/msg-hit-rat-' + this.game.suffix + '.png');
        this.load.image('messageRefill', 'assets/msg-refill-' + this.game.suffix + '.png');

        // For Share Screen
        this.load.image('share-screen-bg', 'assets/scoreboard-howto-bg-' + this.game.suffix + '.jpg');

        this.load.image('btn-share-twitter', 'assets/btn-share-twitter-' + this.game.suffix + '.png');
        this.load.image('btn-share-facebook', 'assets/btn-share-facebook-' + this.game.suffix + '.png');
        this.load.image('btn-share-twitter-small', 'assets/btn-share-twitter-small-' + this.game.suffix + '.png');
        this.load.image('btn-share-facebook-small', 'assets/btn-share-facebook-small-' + this.game.suffix + '.png');
        this.load.image('btn-back', 'assets/btn-back-' + this.game.suffix + '.png');
        this.load.image('title-share', 'assets/title-share-' + this.game.suffix + '.png');

        // For Hud
        this.load.image('hud-level', 'assets/hud-level-' + this.game.suffix + '.png');
        this.load.image('hud-score', 'assets/hud-score-' + this.game.suffix + '.png');
        this.load.image('hud-beer-meter', 'assets/hud-beer-meter-' + this.game.suffix + '.png');
        this.load.image('hud-pause', 'assets/hud-pause-' + this.game.suffix + '.png');
        this.load.image('hud-health', 'assets/hud-health-' + this.game.suffix + '.png');
        this.load.image('hud-health-bar', 'assets/hud-health-bar-' + this.game.suffix + '.png');

        this.load.image('gameOver', 'assets/msg-gameover-' + this.game.suffix + '.png');
        this.load.image('btn-restart', 'assets/btn-restart-' + this.game.suffix + '.png');
    },
    create: function() {
        this.asset.cropEnabled = false;
    },
    update: function() {
        if (!!this.ready) {
            this.game.state.start('menu');
        }
    },
    onLoadComplete: function() {
        this.ready = true;
    }
};



module.exports = Preload;
