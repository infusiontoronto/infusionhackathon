'use strict';

function Share() {}
Share.prototype = {
    preload: function() {
        // Override this method to add some load operations. 
        // If you need to use the loader, you may need to use them here.
    },
    create: function() {

        this.shareButtonGroup = this.game.add.group();
        this.bg = this.game.add.sprite(0, 0, 'share-screen-bg');
        this.shareButtonGroup.add(this.bg);

        this.titleShare = this.game.add.image(this.game.width / 2, this.game.height / 4, 'title-share');
        this.titleShare.anchor.setTo(0.5);
        this.shareButtonGroup.add(this.titleShare);

        this.shareTwitter = this.game.add.button(this.game.width / 2, this.titleShare.y + this.titleShare.height + 150, 'btn-share-twitter', this.shareTwitterClick, this);
        this.shareTwitter.anchor.setTo(0.5);
        this.shareButtonGroup.add(this.shareTwitter);

        this.shareFacebook = this.game.add.button(this.game.width / 2, this.shareTwitter.y + this.shareTwitter.height + 50, 'btn-share-facebook', this.shareFacbookClick, this);
        this.shareFacebook.anchor.setTo(0.5);
        this.shareButtonGroup.add(this.shareFacebook);

        this.backButton = this.game.add.button(50, this.game.height - 180, 'btn-back', this.backButtonClick, this);
        this.shareButtonGroup.add(this.backButton);

    },
    update: function() {
        // state update code
    },
    paused: function() {
        // This method will be called when game paused.
    },
    render: function() {
        // Put render operations here.
    },
    shutdown: function() {
        // This method will be called when the state is shut down 
        // (i.e. you switch to another state from this one).
    },
    shareTwitterClick: function() {
        //share on twitter
        var tweetbegin = 'http://twitter.com/home?status=';
        var tweettxt = 'I like this game Beer Run -' + window.location.href + '.';
        var finaltweet = tweetbegin + encodeURIComponent(tweettxt);
        window.open(finaltweet, '_blank');
    },
    shareFacbookClick: function() {
        // var shareLink = 'https://www.facebook.com/sharer/sharer.php?u=http://infusiondevelop.com'
        var shareLink = "http://www.facebook.com/sharer.php?s=100&amp;p[title]=Beer Run!&amp;p[summary]=Check out Beer Run by the Interactive Development team at Infusion Toronto&amp;p[url]=www.infusion.com&amp;p[images][0]=http://infusion.com/Content/css/logo.png"
        window.open(shareLink, '_blank');
    },
    backButtonClick: function() {
        this.game.state.start('menu');
    }
};
module.exports = Share;
