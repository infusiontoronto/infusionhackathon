'use strict';

function PauseState() {}
PauseState.prototype = {
    preload: function() {
        // Override this method to add some load operations. 
        // If you need to use the loader, you may need to use them here.
    },
    create: function() {
        this.titleGroup = this.game.add.group();

        this.bg = this.game.add.sprite(0, 0, 'title-bg');
        this.titleGroup.add(this.bg);

        this.title = this.game.add.sprite(0, 0, 'title-logo');
        this.titleGroup.add(this.title);

        // var tempW = this.bg.width;
        // this.bg.width = this.game.width;
        // var scaleVal = this.bg.scale.x;
        // this.bg.width = tempW;
        // this.titleGroup.scale.setTo(scaleVal, scaleVal);

        this.startButton = this.game.add.button(this.game.width / 2, 300, 'startButton', this.startClick, this);
        this.startButton.anchor.setTo(0.5, 0.5);
    },
    startClick: function() {
        // start button click handler
        // start the 'play' state
        this.game.state.start('play');
    },
    update: function() {
        // state update code
    },
    paused: function() {
        // This method will be called when game paused.
    },
    render: function() {
        // Put render operations here.
    },
    shutdown: function() {
        // This method will be called when the state is shut down 
        // (i.e. you switch to another state from this one).
    }
};
module.exports = PauseState;
